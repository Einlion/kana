from utils.construct import construct_result
import discord
from discord.ext import commands
from utils.checks import owner_check
import os
import re


class Debugger(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.check(owner_check)
    @commands.command()
    async def tail(self, ctx, directory: str, lines: int = 10,  log_group: str = None):
        if not log_group:
            log_group = directory
        directories = os.listdir("logs")
        if directory in directories:
            curr_dir = f"logs/{directory}"
            nested_directory = os.listdir(curr_dir)
            available_logs = []

            for logfile in nested_directory:
                if logfile.startswith(log_group):
                    available_logs.append(logfile)

            def nat_log_sort(name):
                numbers = re.findall("([0-9]+)", name)
                return int(numbers[-1]) if numbers else 0
            available_logs.sort(key=nat_log_sort)

            if available_logs:
                log_lines = []
                for logfile in available_logs:
                    with open(f"{curr_dir}/{logfile}", "r") as f:
                        temporary_lines = f.read().splitlines()
                        lines_left = lines - len(log_lines)
                        log_lines = temporary_lines[-lines_left:] + log_lines
                        if len(log_lines) >= lines:
                            break

                result = "\n".join(log_lines)
                return await construct_result(ctx, f"```shell\n{result}```")
            else:
                return await ctx.send(f"No logs found in `logs/{directory}` for `{log_group}`.")
        else:
            return await ctx.send(f"Incorrect folder specified. Options are: `{directories}`")


async def setup(client):
    await client.add_cog(Debugger(client))
