import asyncio
from enum import Enum
import logging
from utils.construct import blockify, construct_logger
import discord
from discord.ext import commands, tasks
import asyncpraw
import aiohttp
from datetime import datetime, timezone
from utils.SimplePaginator import SimplePaginator
from utils.checks import owner_check, reddit_mod_check
import json


class RemovalReason(Enum):
    REASON = "reason"
    REPOST = "repost"
    FAQ = "faq"
    ANSWERED = "answered"
    RULE = "rule"

    @classmethod
    def list_options_str(cls) -> str:
        return_str = ""
        for reason in RemovalReason:
            return_str += f"{reason.value}"
        return return_str


class PartialRedditPost:

    """
    There's just too much similar and repeating functions that dict attributes is really confusing. Don't talk to me about no OOP.
    It's a fact that grouping similar functionalities together allows for my sanity to last longer.
    """

    @classmethod
    def init_reddit_instance(cls, reddit_config):
        cls.REDDIT = asyncpraw.Reddit(**reddit_config)

    @classmethod
    async def from_url(cls, url):
        """This is here instead of __init__ because __init__ doesn't do async."""
        self = PartialRedditPost()
        self.submission = await cls.REDDIT.submission(url=url)
        self.is_sub = self.submission.subreddit.display_name.lower() == Sub.NAME
        return self

    async def remove(self, reason: RemovalReason = None, info: str = None):
        assert self.is_sub, f'Submission is in /r/{self.submission.subreddit.display_name}, not /r/{Sub.NAME}'
        reply = None
        if reason == RemovalReason.ANSWERED:
            reply = "Your post seems to contain a question that has been answered."
        elif reason == RemovalReason.FAQ:
            reply = f"You seem to be asking for information that is available either in the [FAQ page](https://www.reddit.com/r/{Sub.NAME}/wiki/faq), the pinned posts, or just by searching in the subreddit."
        elif reason == RemovalReason.ANSWERED:
            reply = "Your post seems to contain a question that has been answered. It was removed to reduce redundancy."
        elif reason == RemovalReason.REPOST:
            if info:
                reply = f"This is a repost: {info}"
            else:
                reply = f"This is a repost."
        elif reason == RemovalReason.RULE and info:
            reply = await Sub.rules(int(info))
        elif reason == RemovalReason.REASON:
            reply = info

        await self.submission.mod.remove()

        if reply:
            await self.submission.mod.send_removal_message(f'This post has been removed for:\n\n>{reply}\n\n*If you think this is a mistake, you can [message the moderators](https://www.reddit.com/message/compose/?to=/r/{Sub.NAME}).*')

        return self.submission.created_utc

    async def approve(self):
        assert self.is_sub, f'Submission is in /r/{self.submission.subreddit.display_name}, not /r/{Sub.NAME}'
        comments = await self.submission.comments()
        await comments.replace_more(limit=0)
        me = await PartialRedditPost.REDDIT.user.me(use_cache=True)
        for top_level_comment in comments:
            if top_level_comment.author == me:
                await top_level_comment.mod.remove()
        await self.submission.mod.approve()

    async def check_repost(self, similarity: int = 85, make_partial: bool = False) -> list:
        """
        Queries RepostSleuth for reposts. Uses media url instead of
        """
        match_list = []
        parameters = {
            "filter": "true",
            "url": self.submission.url,
            "postId": self.submission.id,
            "same_sub": "true",
            "filter_author": "false",
            "only_older": "false",
            "include_crossposts": "false",
            "meme_filter": "false",
            "target_match_percent": similarity,
            "filter_dead_matches": "false",
            "target_days_old": "0"
        }
        async with aiohttp.ClientSession() as session:
            async with session.get("https://api.repostsleuth.com/image", params=parameters) as page:
                if page.status == 200:
                    resp = await page.json()
                    if resp:
                        logging.info(f"{len(resp['matches'])} found")
                        for match in resp["matches"]:
                            if str(match["post"]["subreddit"]).lower() == Sub.NAME:
                                match_list.append(f'https://redd.it/{match["post"]["post_id"]}')
                elif page.status == 400:
                    return match_list
                else:
                    raise Exception(f"RepostSleuth returned code {page.status}. Try again later.")

        match_list = [await PartialRedditPost.from_url(match) for match in match_list]
        filtered_match_list = list(filter(lambda post: post.submission.author is not None and post.submission.id != self.submission.id, match_list))

        return filtered_match_list

    async def check_recent(self):
        params = {
            "q": "",
            "after": "24h",
            "author": self.submission.author.name,
            "sort": "desc",
            "subreddit": Sub.NAME,
            "size": 5
        }
        async with aiohttp.ClientSession() as session:
            async with session.get("https://api.pushshift.io/reddit/search/submission/", params=params) as page:
                if page.status == 200:
                    resp = await page.json()
                    if resp["data"]:
                        return [f"https://redd.it/{post['id']}" for post in resp["data"] if not post["is_self"]]
                else:
                    return []

    def embed(self, override_title: str = None):
        em = discord.Embed(
            title=self.submission.title,
            url=self.submission.url,
            color=0xff6600,
        )
        em.add_field(name="Score", value=self.submission.score)
        em.add_field(name="Comments", value=self.submission.num_comments)
        em.add_field(name="Posted", value=datetime.utcfromtimestamp(self.submission.created_utc).strftime('%d-%m-%Y %X'))
        em.description = f"[Comments]({self.submission.shortlink})\n"
        if self.submission.is_self:
            if self.submission.selftext:
                em.description += blockify(self.submission.selftext[:200])
                if len(self.submission.selftext.split(" ")) > 200:
                    em.description += "..."
        else:
            em.set_image(url=self.submission.url)

        if override_title:
            em.set_author(name=override_title)
        em.set_footer(text=f"Post by: /u/{self.submission.author.name if self.submission.author is not None else '[deleted]'}")
        return em


class Sub:
    """Sub config."""
    @classmethod
    def from_config(cls, config: dict):
        try:
            cls.NAME = config.get("name", None).lower()
            cls.MODS = config.get("mods", None)
            cls.AUTO = cls.Auto()
        except KeyError:
            pass

    @classmethod
    async def init_subreddit_instance(cls):
        cls.INSTANCE = await PartialRedditPost.REDDIT.subreddit(cls.NAME)

    @classmethod
    async def rules(cls, num: int = 0):
        response = ""
        if not hasattr(cls, "INSTANCE"):
            cls.INSTANCE = await PartialRedditPost.REDDIT.subreddit(cls.NAME)
        actual_number = num - 1
        if actual_number < 0:
            async for rule in cls.INSTANCE.rules:
                response += f"{rule.priority+1}: {rule.short_name}\n"
        else:
            rule = await cls.INSTANCE.rules.get_rule(actual_number)
            if rule:
                response += f"Rule {num}: {rule.short_name}\n\n>{rule.description}"
        return response

    class Auto:
        def __init__(self):
            self.interval = 15
            self.is_running = False
            self.is_checking_for_new_posts = True
            self.is_checking_for_reposts = True
            self.is_checking_for_new_modmail = True
            self.destination = None

        def current(self):
            return {
                "interval": self.interval,
                "is_running": self.is_running,
                "is_checking_for_new_posts": self.is_checking_for_new_posts,
                "is_checking_for_reposts": self.is_checking_for_reposts,
                "is_checking_for_new_modmail": self.is_checking_for_new_modmail,
                "destination": self.destination.name
            }

        async def check_new_posts(self):
            new_posts = []
            if not hasattr(Sub, "INSTANCE"):
                Sub.INSTANCE = await PartialRedditPost.REDDIT.subreddit(Sub.NAME)
            async for post in Sub.INSTANCE.new():
                date_created = datetime.utcfromtimestamp(post.created_utc)
                if int((datetime.utcnow() - date_created).seconds / 60) <= self.interval:
                    p = await PartialRedditPost.from_url(post.shortlink)
                    new_posts.append(p)
                else:
                    break
            return new_posts

        async def check_new_modmail(self):
            new_modmail = []
            if not hasattr(Sub, "INSTANCE"):
                Sub.INSTANCE = await PartialRedditPost.REDDIT.subreddit(Sub.NAME)
            async for message in Sub.INSTANCE.mod.unread():
                date_created = datetime.utcfromtimestamp(message.created_utc)
                mods = await Sub.INSTANCE.moderator()
                if int((datetime.utcnow() - date_created).seconds / 60) <= self.interval and message.author not in mods:
                    new_modmail.append(message)
                    await message.mark_read()
                else:
                    break
            new_modmail_embed = []
            for num, message in enumerate(new_modmail, start=1):
                em = discord.Embed(
                    title=f"Showing unread modmail {num}/{len(new_modmail)} from /r/{Sub.NAME}",
                    description=f"[Check inbox](https://mod.reddit.com/mail/inbox)",
                    color=0xff6600
                )
                em.add_field(name="Sent by", value=message.author.name)
                em.add_field(name="Datetime (UTC)", value=date_created.strftime('%d-%m-%Y %X'))
                em.add_field(name="Subject", value=blockify(f"{message.subject[:50]}{'...' if len(message.subject)>=50 else ''}"), inline=False)
                em.add_field(name="Content", value=blockify(f"{message.body[:300]}{'...' if len(message.body)>=300 else ''}"), inline=False)
                new_modmail_embed.append(em)
            return new_modmail_embed


class RedditMod(commands.Cog):

    def __init__(self, client):
        self.client = client
        self.logger = construct_logger(file_path="logs/info/reddit_mod.log")
        PartialRedditPost.init_reddit_instance(self.client.config["reddit"])
        Sub.from_config(self.client.config["subreddit"])

    @commands.command()
    async def peek(self, ctx, url):
        post = await PartialRedditPost.from_url(url)
        return await ctx.reply(embed=post.embed())

    @commands.command()
    async def rule(self, ctx, num: int = 0):
        response = await Sub.rules(num)
        return await ctx.reply(blockify(response, form="md"))

    # command to remove

    @commands.command()
    async def remove(self, ctx, url: str, reason: str = None, *, info: str = None):
        """Remove a post. Command: remove <url> <reason> <info>."""
        post = await PartialRedditPost.from_url(url)
        reason = None if reason is None else RemovalReason(reason.lower())
        ts = await post.remove(reason, info)
        if ts:
            return await ctx.send(blockify(f"Post removed.\nTime since post creation: {datetime.utcnow()-datetime.utcfromtimestamp(ts)}"))

    # command to approve

    @commands.command()
    async def approve(self, ctx, url: str):
        """Approve a post."""
        post = await PartialRedditPost.from_url(url)
        await post.approve()
        return await ctx.send(blockify("Post approved."))

    # command to check repost

    @commands.command()
    async def repost(self, ctx, url: str, similarity: int = 85):
        """Check if the media object in a post is a repost."""
        post = await PartialRedditPost.from_url(url)
        self.logger.info(f"Finding reposts of {url}.")
        match_list = await post.check_repost(similarity=similarity)
        self.logger.info(f"{len(match_list)} instances found.")
        if match_list:
            if len(match_list) > 1:
                return await SimplePaginator(extras=[match.embed() for match in match_list]).paginate(ctx)
            else:
                return await ctx.send(embed=match_list[0].embed())
        else:
            return await ctx.send(blockify("No similar posts found!"))

    # command to check recent post

    # command to auto

    @commands.check(owner_check)
    @commands.group(invoke_without_command=True)
    async def auto(self, ctx, *args):
        reply = f"Auto is {'on' if Sub.AUTO.is_running else 'off'}."
        if self.run_tasks.next_iteration:
            reply += f"\nNext iteration occurs in {(self.run_tasks.next_iteration - datetime.now(timezone.utc)).total_seconds() // 60 } minutes."
        if args:
            reply += "\nCurrent settings: "
            reply += json.dumps(Sub.AUTO.current(), indent=4)
        return await ctx.send(blockify(reply))

    @auto.command(invoke_without_command=True, aliases=["on"])
    @commands.check(reddit_mod_check)
    async def start(self, ctx, *args):
        Sub.AUTO.is_running = True
        Sub.AUTO.destination = ctx.channel
        # override tasks deco interval with self interval thing:
        self.run_tasks.change_interval(minutes=Sub.AUTO.interval)
        if args:
            if args[0] in ("new"):
                Sub.AUTO.is_checking_for_new_posts = True
            elif args[0] in ("modmail"):
                Sub.AUTO.is_checking_for_new_modmail = True
            elif args[0] in ("repost", "reposts"):
                Sub.AUTO.is_checking_for_reposts = True
        else:
            self.run_tasks.start()

        return await ctx.send(blockify("Turned on auto."))

    @auto.command(invoke_without_command=True, aliases=["off"])
    @commands.check(reddit_mod_check)
    async def stop(self, ctx, *args):
        if args:
            if args[0] in ("new"):
                Sub.AUTO.is_checking_for_new_posts = False
            elif args[0] in ("modmail"):
                Sub.AUTO.is_checking_for_new_modmail = False
            elif args[0] in ("repost", "reposts"):
                Sub.AUTO.is_checking_for_reposts = False
        else:
            Sub.AUTO.is_running = False
            self.run_tasks.cancel()
        return await ctx.send(blockify("Turned off auto."))

    @auto.command(invoke_without_command=True)
    @commands.check(reddit_mod_check)
    async def restart(self, ctx):
        self.run_tasks.restart()
        return await ctx.send(blockify("Restarted."))

    @tasks.loop(minutes=15)
    async def run_tasks(self):
        # modmail check
        if Sub.AUTO.is_checking_for_new_modmail:
            self.logger.info("Checking for new modmail.")
            new_modmail = await Sub.AUTO.check_new_modmail()
            self.logger.info("Checking for new modmail complete.")
            if new_modmail:
                self.logger.info(f"{len(new_modmail)} new modmail found.")
                for modmail in new_modmail:
                    await Sub.AUTO.destination.send(embed=modmail)
                    await asyncio.sleep(.25)

        # post check
        self.logger.info("Checking for new posts.")
        new_posts = await Sub.AUTO.check_new_posts()
        self.logger.info("Checking for new posts complete.")
        if new_posts:
            self.logger.info(f"{len(new_posts)} new posts found.")
            for post in new_posts:
                if Sub.AUTO.is_checking_for_new_posts:
                    em = post.embed(override_title=f"/r/{Sub.NAME} - New")
                    await Sub.AUTO.destination.send(embed=em)
                    await asyncio.sleep(.25)

                # repost check

                if Sub.AUTO.is_checking_for_reposts and not post.submission.is_self:
                    self.logger.info(f"Finding reposts of {post.submission.shortlink}.")
                    match_list = await post.check_repost()
                    self.logger.info(f"{len(match_list)} instances found.")
                    if match_list:
                        self.logger.info(f"Removing post {post.submission.shortlink}")
                        ts = await post.remove(RemovalReason.REPOST, match_list[0].submission.shortlink)
                        if ts:
                            await Sub.AUTO.destination.send(blockify(f"[AUTO] Post {post.submission.shortlink} removed for being a repost of {match_list[0].submission.shortlink}.\nTime since post creation: {datetime.utcnow()-datetime.utcfromtimestamp(ts)}"))
                            await Sub.AUTO.destination.send(embed=post.embed())
                            await Sub.AUTO.destination.send(embed=match_list[0].embed())
                            self.logger.info(f"Completed removal of post {post.submission.shortlink}.")

                    else:
                        pass

                        # recent post check

                        # command to post

                        # command to reply

                        # command to pin

                        # raw reaction add action?


async def setup(client):
    await client.add_cog(RedditMod(client))
