import os
from discord import File
from logging import handlers
import logging


def blockify(text: str, form: str = None) -> str:
    return f"```{text}```" if form is None else f"```{form}\n{text}\n```"


def construct_path(*args) -> str:
    path_name = ""
    for a in args:
        path_name = os.path.join(path_name, a)
    return path_name


async def construct_result(ctx, result):
    if len(str(result)) > 2000:
        with open(f"tmp/{ctx.message.id}.txt", "w") as f:
            f.write(str(result.strip("```")))
        with open(f"tmp/{ctx.message.id}.txt", "rb") as f:
            py_output = File(f, "output.txt")
            await ctx.send(content="uploaded output to file since output was too long.", file=py_output)
            os.remove(f"tmp/{ctx.message.id}.txt")
    else:
        await ctx.send(result)


def construct_handler(*, file_path, level: int = logging.INFO, max_bytes: int = 500000, backup_count: int = 10, formatter: logging.Formatter = None):
    filepath, _ = os.path.split(file_path)
    os.makedirs(filepath, exist_ok=True)
    f = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s") if not formatter else formatter
    logHandler = handlers.RotatingFileHandler(
        file_path, maxBytes=max_bytes, backupCount=backup_count, encoding="utf-8"
    )
    logHandler.setLevel(level)
    logHandler.setFormatter(f)
    return logHandler


def construct_logger(*, file_path, level: int = logging.INFO, max_bytes: int = 500000, backup_count: int = 10, formatter: logging.Formatter = None):

    filepath, filename = os.path.split(file_path)
    os.makedirs(filepath, exist_ok=True)
    name = os.path.splitext(filename)[0]

    logger = logging.getLogger(name)
    logger.setLevel(level)

    # log formatter
    f = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s") if not formatter else formatter
    logHandler = construct_handler(file_path=file_path, level=level, max_bytes=max_bytes, backup_count=backup_count, formatter=f)

    # fixes bug when bot restarted but log file retained loghandler. this will remove any handlers it already had and replace with new ones initialized above
    for hdlr in list(logger.handlers):
        logger.removeHandler(hdlr)
    logger.addHandler(logHandler)

    return logger
