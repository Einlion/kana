import discord
from discord.ext import commands
import os
from utils.checks import owner_check
import subprocess
import asyncio
from utils.dataIO import dataIO
from utils.construct import construct_handler, construct_path
import logging
import traceback
import sys


CONFIG_PATH = construct_path("settings", "config.json")
CONFIG = dataIO.load_json(CONFIG_PATH)
PREFIX = CONFIG["botsugi_settings"]["prefix"]

intents = discord.Intents.default()
intents.message_content = True

class Kana(commands.Bot):
    async def setup_hook(self):
        extensions = [
            f"cogs.{extension[:-3]}"
            for extension in os.listdir("cogs")
            if extension.endswith(".py")
        ]
        for extension in extensions:
            try:
                await self.load_extension(extension)
                print(extension)
            except Exception as e:
                print(
                    "Failed to load extension {}\n{}: {}".format(
                        extension, type(e).__name__, e
                    )
                )
                traceback.print_exc()
    
kana = Kana(command_prefix=commands.when_mentioned_or(PREFIX), intents=intents)
kana.config = CONFIG

@kana.event
async def on_ready():
    kana.config = dataIO.load_json(CONFIG_PATH)
    os.makedirs("tmp", exist_ok=True)

    if os.path.isfile("restart.json"):
        restart = dataIO.load_json("restart.json")
        try:
            guild = kana.get_guild(restart["guild"])
            channel = guild.get_channel(restart["channel"])
            kana.logger.info(f"Restarted in {channel.name}.")
            await channel.send("```Restarted.```")
        except:
            tb = traceback.format_exc()
            kana.logger.error(tb)
            logging.error(tb)
            print(tb)
        finally:
            os.remove("restart.json")

    kana.logger.info(f"Logged in as: {kana.user.name}")
    logging.info(f"Logged in as: {kana.user.name}")


@kana.before_invoke
async def before_any_command(ctx):
    kana.logger.info(f"Command invoked : {ctx.command} | By user: {str(ctx.author)} | In server: {ctx.guild} | In channel: {ctx.channel} | Message: {ctx.message.content}")


@kana.after_invoke
async def after_any_command(ctx):
    kana.logger.info(
        f"Command finished: {ctx.command} | By user: {str(ctx.author)} | In server: {ctx.guild} | In channel: {ctx.channel} | Message: {ctx.message.content}")


@kana.event
async def on_command_error(ctx, error):
    if isinstance(error, commands.errors.CommandNotFound):
        pass
    else:
        await ctx.reply(f"```An error has occured: {error}```")
        trace = traceback.format_exception(type(error), error, error.__traceback__)
        trace_str = "".join(trace)
        kana.logger.error(trace_str)
        print(trace_str)


@kana.event
async def on_error(event, *args, **kwargs):
    exc_type, _, _ = sys.exc_info()
    print(exc_type)
    if isinstance(exc_type, discord.errors.ConnectionClosed) or isinstance(exc_type, discord.ConnectionClosed) or issubclass(exc_type, discord.errors.ConnectionClosed) or issubclass(exc_type, discord.ConnectionClosed) or issubclass(exc_type, ConnectionResetError):
        kana.logger.error(f"[CRASHED] {exc_type}")
        print("[ERROR] Exception occured, restarting bot...")
        kana.logger.error("[ERROR] Exception occured, restarting bot...")
        bot_exit()
    else:
        trace = traceback.format_exc()
        kana.logger.error(f"[ERROR] {trace}")
        print(trace)


def bot_exit():
    os._exit(0)


@kana.command(aliases=["exit"])
@commands.check(owner_check)
async def quit(ctx):
    with open('quit.txt', 'w', encoding="utf8") as f:
        f.write('.')
    await ctx.send("```Shutting down.```")
    kana.logger.info("Shut down.")
    print("[SHUTDOWN] Ending processes...")
    bot_exit()


@kana.command(aliases=["reboot"])
@commands.check(owner_check)
async def restart(ctx, txt: str = None):
    """Restarts the bot. Pulls if necessary."""
    if txt and txt.lower() in ("pull"):
        await ctx.send("```Pulling from repo before shutdown...```")
        process = subprocess.Popen(["git", "pull"], stdout=subprocess.PIPE)
        loop = asyncio.get_event_loop()
        output = await loop.run_in_executor(None, process.communicate)
        await ctx.send(f"```{output[0].decode(encoding='utf8', errors='ignore')}```")
    await ctx.send(f"```Restarting...```")
    restart = {"guild": ctx.guild.id, "channel": ctx.channel.id}
    dataIO.save_json("restart.json", restart)
    dataIO.save_json(CONFIG_PATH, kana.config)
    bot_exit()


@kana.command()
async def cute(ctx):
    """Checks if the bot is responsive."""
    await ctx.reply("Kana is the cutest!")


async def start_bot(token):
    logger = logging.getLogger("discord")
    logger.setLevel(logging.INFO)

    log_handler = construct_handler(file_path="logs/info/info.log", level=logging.INFO)
    error_log_handler = construct_handler(file_path="logs/error/error.log", level=logging.ERROR)

    logger.addHandler(log_handler)
    logger.addHandler(error_log_handler)

    async with kana as bot:
        bot.logger = logger
        await bot.start(token)


try:
    asyncio.run(start_bot(CONFIG["discord"]["token"]))
except KeyboardInterrupt:
    pass
